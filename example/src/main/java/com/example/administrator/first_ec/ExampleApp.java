package com.example.administrator.first_ec;

import android.app.Application;

import com.example.latte_core.app.Latte;
import com.example.latte_ec.icon.FontEcModule;
import com.joanzapata.iconify.fonts.FontAwesomeModule;

/**
 * Created by Administrator on 2018/8/13 0013.
 * 项目名 ： First_EC
 * 包名 ： com.example.administrator.first_ec
 * 文件名 ：ExampleApp
 * 创建者 ：wzp
 * 创建时间 ： 八月
 * 描述 ： TODO
 */
public class ExampleApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Latte.init(this)
                .withApiHost("http://127.0.0.1")//回环 本地
                .withIcon(new FontAwesomeModule())
                .withIcon(new FontEcModule())
                .configure();


    }
}
