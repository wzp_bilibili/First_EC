package com.example.administrator.first_ec;

import android.os.TokenWatcher;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.latte_core.app.Latte;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * 初始化OK传入了上下文到LATTE_CONFIGS的hashmap中
         * **/
        Toast.makeText(Latte.getApplication(),
                Latte.getApplication().toString()+"传入Context",Toast.LENGTH_LONG).show();

    }
}
