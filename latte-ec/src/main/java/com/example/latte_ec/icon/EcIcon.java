package com.example.latte_ec.icon;

import com.joanzapata.iconify.Icon;

/**
 * Created by Administrator on 2018/8/13 0013.
 */
 public enum EcIcon implements Icon {
     icon_alipay('\ue60e'),
    ;
    private  char character;


    EcIcon(char character) {
        this.character = character;
    }


    @Override
    public String key() {
        return name().replace("_","-");
    }

    @Override
    public char character() {
        return character;
    }
}
