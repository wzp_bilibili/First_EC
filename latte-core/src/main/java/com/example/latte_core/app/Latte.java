package com.example.latte_core.app;

import android.content.Context;

import java.util.HashMap;
import java.util.WeakHashMap;

/**
 * Created by Administrator on 2018/8/13 0013.
 * 项目名 ： First_EC
 * 包名 ： com.example.latte_core.app
 * 文件名 ：Latte
 * 创建者 ：wzp
 * 创建时间 ： 八月
 * 描述 ： TODO
 */
public final class Latte {

    public static Configurator init(Context context){
        getConfigurations().put(ConfigType.APPLICATION_CONTEXT.name(),context.getApplicationContext());
        return  Configurator.getInstance();
    }

    private static HashMap<String,Object> getConfigurations(){

        return Configurator.getInstance().getLatteConfigs();
   }
    public static Context getApplication(){
        return (Context) getConfigurations().get(ConfigType.APPLICATION_CONTEXT.name());
  }
}
