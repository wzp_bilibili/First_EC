package com.example.latte_core.app;

import android.util.Log;

import com.joanzapata.iconify.IconFontDescriptor;
import com.joanzapata.iconify.Iconify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;

/**
 * Created by Administrator on 2018/8/13 0013.
 * 项目名 ： First_EC
 * 包名 ： com.example.latte_core.app
 * 文件名 ：Configurator
 * 创建者 ：wzp
 * 创建时间 ： 八月
 * 描述 ：  管理配置项类
 */
public class Configurator {
     private static final  String TAG = "wzp_Configurator";

     private static final HashMap<String,Object> LATTE_CONFIGS =  new HashMap<>();
     /**
      * iconify 存储数据集
      * **/
     private static final ArrayList<IconFontDescriptor>  ICONS = new ArrayList<>();

    /**
     * 初始化
     * **/
    private Configurator() {

        /**
         * .name() 以字符串的形式输出出来 和tostring默认实现是一样的 tostring 可以重写 name 不可以
         *  name变量就是枚举变量的字符串形式
         * */
        LATTE_CONFIGS.put(ConfigType.CONFIG_READY.name(),false);

    }

    /**x
     * 线程安全懒汉模式
     *
     * 静态内部类实现单例 初始化
     * **/
    public static Configurator getInstance (){
        return Hodler.INSTANCE;
    }

    private static class Hodler{
        private static final Configurator INSTANCE = new Configurator();
    }

    final HashMap<String , Object> getLatteConfigs(){
        return LATTE_CONFIGS;
    }

    public final void configure(){
        initIcons();
        LATTE_CONFIGS.put(ConfigType.CONFIG_READY.name(),true);
    }

    /**
     *
     * api host
     *
     * **/

    public final Configurator withApiHost(String host){
        LATTE_CONFIGS.put(ConfigType.API_HOST.name(),host);
        return  Configurator.this;
    }
    /**
     * 字体图标
     *
     * **/
    public final Configurator withIcon(IconFontDescriptor iconFontDescriptor){

        Log.i(TAG,"IconFontDescriptor="+iconFontDescriptor.ttfFileName());
        ICONS.add(iconFontDescriptor);
        return this;
    }

    /**
     *检查配置项是否完成
     *
     * tip:写类变量和方法变量时 让他的变量不可变性到达最大化
     * **/
    private void checkConfiguration(){
        final boolean isReady = (boolean) LATTE_CONFIGS.get(ConfigType.CONFIG_READY.name());
        if (!isReady){
            throw new RuntimeException("Configuration is not ready , call configure");
        }
        }

    final <T> T getConfiguration(Enum<ConfigType> key){
        checkConfiguration();
        return (T) LATTE_CONFIGS.get(key.name());
    }

    /**
     * 初始化iconify
     * **/
    private void initIcons(){
        Log.i(TAG,"initicons="+ICONS.size());
        if (ICONS.size() > 0){
          final  Iconify.IconifyInitializer initializer = Iconify.with(ICONS.get(0));
            for (int i = 1 ; i < ICONS.size() ; i++){
                initializer.with(ICONS.get(i));
            }
        }
    }
}

