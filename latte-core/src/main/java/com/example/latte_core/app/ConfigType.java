package com.example.latte_core.app;

/**
 * Created by Administrator on 2018/8/13 0013.
 * 项目名 ： First_EC
 * 包名 ： com.example.latte_core.app
 * 文件名 ：ConfigType
 * 创建者 ：wzp
 * 创建时间 ： 八月
 * 描述 ： 枚举类 整个应用程序中是唯一的单例 并且只能被初始化一次
 */
public enum ConfigType {
    /**
     *  API_HOST 配置网络请求的域名
     *  APPLICATION_CONTEXT 全局上下文
     *  CONFIG_READY 控制初始化和配置完成了没有
     *  ICON 字体的初始化项目
     * **/
    API_HOST,
    APPLICATION_CONTEXT,
    CONFIG_READY,
    ICON
}
